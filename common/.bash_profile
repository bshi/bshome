SYSTEM=$( uname -s | tr '[:upper:]' '[:lower:]' )

# ls Colors
export CLICOLOR=1
if [ "${SYSTEM}" = "linux" ]; then
  # GNU
  export LS_COLORS="di=36;40:ln=35;40:so=32;40:pi=33;40:ex=31;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:"
else
  # BSD, etc
  export LSCOLORS=gxfxcxdxbxegedabagacad
fi

for bindir in "${HOME}/bin" "${HOME}/google-cloud-sdk/bin" "${HOME}/.local/bin"; do
  if [ -d "${bindir}" ] ; then
    export PATH="${bindir}:${PATH}"
  fi
done

export EDITOR=vim

if [ -n "$BASH_VERSION" ]; then
  if [ -f "$HOME/.bashrc" ]; then
  . "$HOME/.bashrc"
  fi
fi

if [ -d "$HOME/.bash_profile.d" ]; then
  for f in `ls $HOME/.bash_profile.d/*`; do
    echo $f
    . $f
  done
fi

if [ -f "/etc/bebop-workstation" ]; then
  # https://git-scm.com/book/en/v2/Git-Internals-Environment-Variables#Committing
  # .gitconfig contains personal email, make sure we use the right email
  # address in work environments.
  export GIT_AUTHOR_NAME="Bo Shi"
  export GIT_AUTHOR_EMAIL="bshi@google.com"
  export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
  export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
fi

if hash tmux 2>/dev/null; then
  echo '~ tmux sessions ~'
  tmux ls
else
  echo '~ tmux not installed ~'
fi
