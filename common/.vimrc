execute pathogen#infect()

set nowrap
set hlsearch
set nocompatible
set modeline
filetype plugin indent on

""" CtrlP filters
let g:ctrlp_custom_ignore = '\v[\/](\.(git|hg|svn)|target)$'

let g:ctrlp_cache_dir = $HOME . '/.vim_run/ctrlp'

if executable('ag')
  """ silversurfer-ag (debian) installed, use for search.
  let g:ctrlp_user_command = 'ag %s -p .ignore -l --nocolor -g ""'
endif

""" Some more esoteric file extensions
au BufNewFile,BufRead *.aci set filetype=json   				""" https://github.com/appc/spec
au BufNewFile,BufRead *.bzl,*.bazel,BUILD,WORKSPACE set ft=bzl	""" https://github.com/bazelbuild/vim-ft-bzl/issues/8
au BufNewFile,BufRead *.dart set ft=dart

augroup autoformat_settings
  autocmd FileType bzl AutoFormatBuffer buildifier
  autocmd FileType c,cpp,proto,javascript,arduino AutoFormatBuffer clang-format
  autocmd FileType dart AutoFormatBuffer dartfmt
augroup END

set ts=2 sw=2

""" Command line behavior
set wildignore=*.o,*.so,*~,*.pyc,*.class


""" Colors
syntax on
colorscheme torte
" Your .profile or .bashrc requires "export TERM=xterm-256color" for the
" SignColumn coloring to not be gray.
highlight clear SignColumn


""" Navigation
set relativenumber

set directory=$HOME/.vim_run/swap//
set backupdir=$HOME/.vim_run/backup//

""" GUI
if has("gui_running")
  set go-=T go-=r go-=L
endif

let g:indentLine_char = '.'

if filereadable(glob("~/.vimrc.local"))
  source ~/.vimrc.local
endif
